var express = require('express');
var jwt = require('jsonwebtoken');
var cors = require('cors');
  app = express();
  router = express.Router();
  mongoose = require('mongoose');

module.exports = function (app) {
  app.use(cors());
  app.use('/', router);
};

router.get('/', function(req, res, next) {
  res.redirect('login')
});

//Login Page
router.get('/login', function (req, res, next) {
    res.render('login', {
      title: 'ErixSoekamti.com - Login',
      layout: 'login',
      scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

//Register Page
router.get('/register', function (req, res, next) {
    res.render('register', {
      title: 'ErixSoekamti.com - Register',
      layout: 'login',
      scripts: '<script type="text/javascript" src="/js/register.js"></script>'
    });
});

//Activation User
router.get('/activate/:activation_code', function (req, res, next) {
  console.log(req.params.activation_code);
  console.log("oke");
    request({
      method:'PUT',
      uri:'http://192.168.0.26:3000/v1/activate'+req.params.activation_code,
      function(error, response, body){
      }
    });
    res.render('/login');
});

//Forgot Password
router.get('/forgotpassword', function (req, res, next) {
    res.render('forgotpassword', {
      title: 'ErixSoekamti.com - Forgot Password',
      layout: 'login',
      scripts: '<script type="text/javascript" src="/js/forgotpassword.js"></script>'
    });
});

//Home Page
router.get('/home', function (req, res, next) {
    res.render('index', {
      title: 'ErixSoekamti.com - Home',
      scripts: '<script type="text/javascript" src="/js/main.js"></script>'
    });
});

//User Page
router.get('/user', function (req, res, next) {
    res.render('user', {
      title: 'ErixSoekamti.com - user',
      layout: 'searchuser',
      scripts: '<script type="text/javascript" src="/js/user.js"></script>'
    });
});

//Profile-Show by Name
router.get('/searchuser/:name', function (req, res, next) {
      res.render('searchuser', {
      title: 'ErixSoekamti.com - searchuser',
      layout: 'searchuser',
      scripts: '<script type="text/javascript" src="/js/searchuser.js"></script>'
    });
});
//Logout nggak butuh router