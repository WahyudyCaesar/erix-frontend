function initMap() {
  var uluru = {lat: -7.793181, lng: 110.381294};
  var peta = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: uluru
  });
  var marker = new google.maps.Marker({
    position: uluru,
    map: peta
  });
}