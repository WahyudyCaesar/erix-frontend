$('#name').keydown(function(event){
  if(event.keyCode ==13) {
    $("#password").focus();
    return false;
  };
});

// $("password").html("");

$("#password").keydown(function(event){
    if(event.keyCode == 13){
        $("#loginsubmit").click();
        return false;
    }
});

//fungsi baru untuk bikin json parse
(function($){
    $.fn.serializeObject = function(){

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push":     /^$/,
                "fixed":    /^\d+$/,
                "named":    /^[a-zA-Z0-9_]+$/
            };

        this.build = function(base, key, value){
            base[key] = value;
            return base;
        };

        this.push_counter = function(key){
            if(push_counters[key] === undefined){
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function(){

            // skip invalid keys
            if(!patterns.validate.test(this.name)){
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while((k = keys.pop()) !== undefined){

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if(k.match(patterns.push)){
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if(k.match(patterns.fixed)){
                    merge = self.build([], k, merge);
                }

                // named
                else if(k.match(patterns.named)){
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);

//end function

$('#formlogin').validate({
	rules: {
		name: {
			required: true,
			minlength: 2
		},
		password: {
			required: true,
			minlength: 6
		}
	},
	messages: {
		name: {
			required: "Nama harus diisi",
			minlength: "Name harus lebih dari 2 karakter"
		},
		password: {
			required: "Password harus diisi",
			minlength: "Password harus lebih dari 6 karakter"
		}
	}
});

$('#loginsubmit').on('click', function(e){
	if($("#formlogin").valid()==true){
		var dataSubmited = $('#formlogin').serializeObject();
		console.log("iniiiii" + dataSubmited);
		e.preventDefault();
	    $.ajax({
           type: "POST",
           url: "http://192.168.0.131:3000/v1/login", //=
           data: {
              name:dataSubmited.name,
              password:dataSubmited.password,
              role:{
                id:1,
                name: "login"
              }
          },
	       success: function(data) {
            // console.log(data.token);
            // localStorage.setItem('server',data.token);
            localStorage.setItem('token', data.token);
            console.log("ini " + localStorage.token);
            // document.getElementById("formlogin").reset();
            if (localStorage.token == data.token) {
                // alert('Thank you For Login.');
                window.location.href="http://localhost:3000/home";
            }
           },
           error: function(err){
            result = JSON.parse(err.responseText); //get response terus dijsonin dari backend
            alert('Frontend Nama / Email tidak terdaftar');
            window.location.href="http://localhost:3000/login";
           },
        });
    } else {
        console.log('Password Anda Salah');
	}
});

