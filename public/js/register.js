function next(x,y){
  $(x).keydown(function(event){
    if(event.keyCode ==13) {
      $(y).focus();
      return false;
    };
  });
};

$(".username").keypress(next('.username', '.jalan'));
$(".jalan").keypress(next('.jalan', '.kota'));
$(".kota").keypress(next('.kota', '.provinsi'));
$(".provinsi").keypress(next('.provinsi', '.datebirth'));
$(".datebirth").keypress(next('.datebirth', '.profession'));
$(".profession").keypress(next('.profession', '.passion'));
$(".passion").keypress(next('.passion', '.telepon'));
$(".telepon").keypress(next('.telepon', '.email'));
$(".email").keypress(next('.email', '.password'));
$(".password").keypress(next('.password', '#registrationsubmit'));

function validasi() {
  var testName = /^[-\w\.\$@\*\!]{1,25}$/
  var name = document.getElementById("name")
  if(testName.test(name.value)) {
    return true;
    }else{
    alert("Nama tidak boleh menggunakan spasi & maksimal 25 karakter");
    name.focus();
  }
}

//--------validasi form request
$('#registrationsubmit').on('click', function(e){
  if($('#formregister').valid()==true && validasi()==true){
    e.preventDefault();
    $.ajax({
       type: "POST",
       url: "http://192.168.0.127:3000/v1/register",         
       data: $('#formregister').serialize(),      
       success: function(data) {
        console.log('berhasil');
       $('#alertRegSuccess').removeClass('hide');// tambahin class hide di form request
        setTimeout(function(){
          $('#alertRegSuccess').addClass('hide');
          window.location.href="http://localhost:3000/login";
        },5000);
       },
       error: function(err){
        console.log('gagal');
        result = JSON.parse(err.responseText); 
        console.log(err)
        $('#alertRegFailed').removeClass('hide');
       }
    });
  }else{
    alert('not valid');
  }
});

